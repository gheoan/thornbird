import { ConfiguresRouter, RouterConfiguration, Router } from "aurelia-router";

export default class implements ConfiguresRouter {
  public router: Router;

  public configureRouter(config: RouterConfiguration, router: Router) {
    this.router = router;
    router.options.compareQueryParams = true;
    config.map([
      {
        route: ['?sort=confidence', ''],
        moduleId: 'comments',
        nav: true,
        name: 'confidence',
        title: 'Best',
      },
      {
        route: '?sort=top',
        moduleId: 'comments',
        nav: true,
        name: 'top',
        title: 'Top',
      },
    ]);
  }
}
