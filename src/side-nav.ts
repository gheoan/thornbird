import { ComponentCreated, TaskQueue } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import SubredditService from './subreddit-service';
import { Subreddit } from './subreddit-model';

export default class implements ComponentCreated {
  static inject = [SubredditService, TaskQueue, Router];

  private service: SubredditService;
  public subreddits?: Array<Subreddit>;
  public router: Router;
  private taskQueue: TaskQueue;

  constructor(service: SubredditService, taskQueue: TaskQueue, router: Router) {
    this.service = service;
    this.taskQueue = taskQueue;
    this.router = router;
  }

  created() {
    this.taskQueue.queueTask(async () => {
      this.subreddits = await this.service.fetchSubreddits('default');
    });
  }
}
