import {
  ConfiguresRouter,
  Router,
  RouteConfig,
  RoutableComponentActivate,
  RouterConfiguration,
  RoutableComponentDetermineActivationStrategy,
  activationStrategy,
} from 'aurelia-router';
import { TaskQueue } from 'aurelia-framework';
import { PostsParameters } from './post-model';
import SubredditService from './subreddit-service';

export default class implements ConfiguresRouter, RoutableComponentDetermineActivationStrategy,
RoutableComponentActivate {
  static inject = [TaskQueue, SubredditService];

  public router: Router;
  private taskQueue: TaskQueue;
  private subService: SubredditService;

  constructor(taskQueue: TaskQueue, subService: SubredditService) {
    this.taskQueue = taskQueue;
    this.subService = subService;
  }

  configureRouter(config: RouterConfiguration, router: Router) {
    this.router = router;
    config.map([
      {
        route: ['', ':sort'],
        href: '/hot',
        moduleId: 'posts',
        nav: true,
        title: 'Hot',
        name: 'hot',
        generationUsesHref: true,
      },
      {
        route: ':sort',
        href: '/new',
        moduleId: 'posts',
        nav: true,
        title: 'New',
        name: 'new',
        generationUsesHref: true,
      },
      {
        route: ':sort',
        href: '/top',
        moduleId: 'posts',
        nav: true,
        title: 'Top',
        name: 'top',
        generationUsesHref: true,
      },
    ]);
  }

  activate({ sub }: PostsParameters, config: RouteConfig): Promise<void> | void {
    /* eslint-disable no-param-reassign */
    if (!config.title) {
      // TODO: when should this execute?
      this.taskQueue.queueTask(async () => {
        // TODO: is sub ever undefined?
        const subreddit = await this.subService.fetchSubredditByName(sub!);
        config.navModel!.setTitle(subreddit.displayName);
      });
    }
    /* eslint-enable no-param-reassign */
  }

  determineActivationStrategy() { // eslint-disable-line class-methods-use-this
    // TODO: why is this needed?
    return activationStrategy.replace;
  }
}
