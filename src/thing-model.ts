/* eslint-disable camelcase */
export interface Thing<T> {
  name: string;
  kind: string;
  data: T;
}

export interface Created {
  created: number;
  created_utc: number;
}

export interface Votable {
  likes: true | false | null;
}
