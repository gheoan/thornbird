import { HttpClient } from "aurelia-fetch-client";
import { Thing } from './thing-model';
import { Listing, ListingTime } from './listing-model';
import { PostSort, PostsParameters, RawPost, Post } from './post-model';

export default class {
  static inject = [HttpClient]
  private http: HttpClient;
  constructor(http: HttpClient) {
    this.http = http;
  }

  public async fetchPosts({
    sort = PostSort.Hot,
    t: time = ListingTime.Hour,
    sub,
  }: PostsParameters): Promise<Array<Post>> {
    let path: string;
    if (sub) {
      path = `r/${sub}/${sort}`;
    } else {
      path = sort;
    }
    let url: string;
    if (sort === PostSort.Controversial || sort === PostSort.Top) {
      const searchParams = new URLSearchParams({
        t: time,
      });
      url = `${path}?${searchParams.toString()}`;
    } else {
      url = path;
    }
    const response = await this.http.fetch(url);
    const postsListing: Listing<Thing<RawPost>> = await response.json();
    return postsListing.data.children
      .map((thing: Thing<RawPost>) => new Post(thing.data))
      .filter((post: Post) => !post.isNsfw);
  }

  private async fetchPostsByID(id: string): Promise<Array<Post>> {
    try {
      const response = await this.http.fetch(`by_id/t3_${id}`);
      const postsListing: Listing<Thing<RawPost>> = await response.json();
      return postsListing.data.children.map((thing: Thing<RawPost>) => new Post(thing.data));
    } catch (error) {
      throw error;
    }
  }

  public async getPost(id: string): Promise<Post> {
    try {
      const posts = await this.fetchPostsByID(id);
      return posts.find((post: Post) => post.id === id)!;
    } catch (error) {
      throw error;
    }
  }
}
