/* eslint-disable camelcase */
import { Created } from './thing-model';

export interface RawAccount extends Created {
  id: string;
  name: string;
}

export class Account {
  public name: string;

  constructor(data: RawAccount) {
    this.name = data.name;
  }
}
