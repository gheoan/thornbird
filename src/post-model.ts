/* eslint-disable camelcase */
import { Created, Votable } from './thing-model';
import { ListingTime } from './listing-model';

export interface RawPost extends Created, Votable {
  title: string;
  url: string;
  is_self: boolean;
  permalink: string;
  author: string;
  subreddit: string;
  id: string;
  selftext: string;
  score: number;
  over_18: boolean;
  num_comments: number;
  domain: string;
}

export class Post {
  id: string;
  title: string;
  url: string;
  isSelf: boolean;
  permalink: string;
  author: string;
  subreddit: string;
  selfText: string;
  score: number;
  created: number;
  isNsfw: boolean;
  commentsCount: number;
  domain: string;
  likes: 1 | 0 | -1;

  constructor(data: RawPost) {
    this.title = data.title;
    this.url = data.url;
    this.isSelf = data.is_self;
    this.permalink = data.permalink;
    this.author = data.author;
    this.subreddit = data.subreddit;
    this.selfText = data.selftext;
    this.score = data.score;
    this.created = data.created_utc * 1000;
    this.isNsfw = data.over_18;
    this.commentsCount = data.num_comments;
    this.domain = data.domain;
    this.id = data.id;
    switch (data.likes) {
      case true:
        this.likes = 1;
        break;
      case false:
        this.likes = -1
        break;
      default:
        this.likes = 0;
        break;
    };
  }
}

export enum PostSort {
  Hot = 'hot',
  New = 'new',
  Rising = 'rising',
  Top = 'top',
  Controversial = 'controversial',
}

export interface PostsParameters {
  sort?: PostSort;
  t?: ListingTime;
  sub?: string;
}
