import { decorators, valueConverter } from 'aurelia-framework';

export default decorators(valueConverter('objectKeys')).on(class {
  toView(value: Object) { // eslint-disable-line class-methods-use-this
    return Reflect.ownKeys(value);
  }
});
