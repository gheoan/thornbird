import * as he from "he";

export class UnescapeHtmlValueConverter {
  toView(value) {
    return he.decode(value);
  }
}

