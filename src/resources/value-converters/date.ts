import { decorators, valueConverter } from 'aurelia-framework';

export default decorators(valueConverter('date')).on(class {
  public toView(value: number): Date { // eslint-disable-line class-methods-use-this
    return new Date(value);
  }
});
