import {
  ComponentAttached,
  ComponentDetached,
  decorators,
  customAttribute,
} from 'aurelia-framework';

export default decorators(customAttribute('bs-dropdown'))
  .on(class implements ComponentAttached, ComponentDetached {
    static inject = [Element];
    constructor(private element: Element) { }

    attached() {
      $(this.element).dropdown();
    }

    detached() {
      $(this.element).dropdown('dispose');
    }
  });
