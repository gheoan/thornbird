import { decorators, customElement, bindable } from "aurelia-framework";
import VoteService from "../../vote-service";
import { Post } from "../../post-model";

export default decorators(customElement('post-compact'), bindable({ name: 'post' })).on(class {
  static inject = [VoteService];

  private voteService: VoteService;
  public post?: Post;
  public vote?: 1 | 0 | -1 = 0;

  constructor(voteService: VoteService) {
    this.voteService = voteService;
  }

  public bind() {
    this.vote = this.post!.likes;
  }

  public async upvote() {
    const previousVote = this.vote!;
    try {
      this.vote = Math.min(previousVote + 1, 1);
      // post is defined because this can only be called after databind
      this.voteService.vote(`t3_${this.post!.id}`, 1);
    } catch (error) {
      this.vote = previousVote;
      throw error;
    }
  }

  public async downvote() {
    const previousVote = this.vote!;
    try {
      this.vote = Math.max(previousVote - 1, -1);
      // post is defined because this can only be called after databind
      this.voteService.vote(`t3_${this.post!.id}`, -1);
    } catch (error) {
      this.vote = previousVote;
      throw error;
    }
  }
})
