import { decorators, bindable, customElement } from 'aurelia-framework';
import { Comment } from '../../comment-model';
import VoteService from "../../vote-service";

export default decorators(bindable('comment'), customElement('comment-list-item')).on(class {
  static inject = [VoteService];

  public comment?: Comment;
  public visible: boolean = true;
  public vote?: 1 | 0 | -1 = 0;
  private voteService: VoteService;

  constructor(voteService: VoteService) {
    this.voteService = voteService;
  }

  public bind() {
    this.vote = this.comment!.likes;
  }

  public toggleVisibility() {
    this.visible = !this.visible;
  }

  public async upvote() {
    const previousVote = this.vote!;
    try {
      this.vote = Math.min(previousVote + 1, 1);
      // post is defined because this can only be called after databind
      this.voteService.vote(`t1_${this.comment!.id}`, 1);
    } catch (error) {
      this.vote = previousVote;
      throw error;
    }
  }

  public async downvote() {
    const previousVote = this.vote!;
    try {
      this.vote = Math.max(previousVote - 1, -1);
      // post is defined because this can only be called after databind
      this.voteService.vote(`t1_${this.comment!.id}`, -1);
    } catch (error) {
      this.vote = previousVote;
      throw error;
    }
  }
});

