import { ViewEngineHooks, View, decorators, viewEngineHooks } from 'aurelia-framework';
import { ListingTime } from '../../listing-model';

export default decorators(viewEngineHooks).on(class implements ViewEngineHooks {
  beforeBind(view: View) { // eslint-disable-line class-methods-use-this
    Object.assign(view.overrideContext, { ListingTime });
  }
});
