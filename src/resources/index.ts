import { FrameworkConfiguration } from 'aurelia-framework';

// eslint-disable-next-line import/prefer-default-export
export function configure(config: FrameworkConfiguration) {
  config.globalResources([
    './value-converters/date',
    './elements/post-link.html',
    './elements/post-compact',
    './hooks/listing-time',
    './value-converters/object-keys',
    './value-converters/unescape-html',
    './elements/comment-list.html',
    './elements/comment-list-item',
    './attributes/bs-dropdown',
  ]);
}
