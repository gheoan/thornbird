import AuthorizationService from './authorization-service';

export default class {
  static inject = [AuthorizationService];

  public authService: AuthorizationService;

  constructor(loginService: AuthorizationService) {
    this.authService = loginService;
  }

  public submit() {
    this.authService.requestUserToken();
  }
}
