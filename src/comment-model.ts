/* eslint-disable camelcase */
import { Created, Thing, Votable } from './thing-model';
import { Listing } from './listing-model';

export interface RawComment extends Created, Votable {
  id: string;
  body: string;
  body_html: string;
  replies: Listing<Thing<RawComment>> | null;
}

export class Comment {
  id: string;
  body: string;
  htmlBody: string;
  created: number;
  replies: Array<Comment> = [];
  likes: 1 | 0 | -1;

  constructor(data: RawComment) {
    this.id = data.id;
    this.body = data.body;
    this.htmlBody = data.body_html;
    this.created = data.created_utc * 1000;
    switch (data.likes) {
      case true:
        this.likes = 1;
        break;
      case false:
        this.likes = -1
        break;
      default:
        this.likes = 0;
        break;
    };
    if (data.replies) {
      this.replies = data.replies.data.children.map(reply => new Comment(reply.data));
    }
  }
}

export enum CommentSort {
  Best = 'confidence',
  New = 'new',
  Old = 'old',
  Random = 'random',
  QA = 'qa',
  Live = 'live',
}

export interface CommentsParameters {
  id: string;
  sort?: CommentSort;
  name?: string;
}
