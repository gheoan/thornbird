import { Aurelia, TaskQueue } from 'aurelia-framework';
import { I18N, Backend, TCustomAttribute } from 'aurelia-i18n';
import environment from './environment';
import AuthorizationService from './authorization-service';
import { HttpClient, HttpClientConfiguration } from 'aurelia-fetch-client';

// eslint-disable-next-line import/prefer-default-export
export async function configure(aurelia: Aurelia) {
  // TODO: catch errors
  const http = aurelia.container.get(HttpClient) as HttpClient;
  http.configure(httpConfig => httpConfig.rejectErrorResponses());
  const authService = aurelia.container.get(AuthorizationService) as AuthorizationService;
  await authService.authorize();

  aurelia.use
    .standardConfiguration()
    .feature('resources');

  aurelia.use.globalResources('app.css');
  aurelia.use.plugin('@aurelia-ux/core');
  aurelia.use.plugin('@aurelia-ux/button');
  aurelia.use.plugin('@aurelia-ux/icons');

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.use.plugin('aurelia-i18n', (instance: I18N) => {
    const aliases = ['t', 'i18n'];
    // add aliases for 't' attribute
    TCustomAttribute.configureAliases(aliases);

    // register backend plugin
    instance.i18next.use(Backend.with(aurelia.loader));

    // adapt options to your needs (see http://i18next.com/docs/options/)
    // make sure to return the promise of the setup method, in order to guarantee proper loading
    // TODO: catch errors
    return instance.setup({
      debug: environment.debug,
      lng: 'en',
      fallbackLng: 'en',
      backend: { // <-- configure backend settings
        loadPath: './locales/{{lng}}/{{ns}}.json', // <-- XHR settings for where to get the files from
      },
      attributes: aliases,
    })
      .then(() => {
        const taskQueue: TaskQueue = aurelia.container.get(TaskQueue);
        const i18n = aurelia.container.get(I18N);
        taskQueue.queueTask(() => {
          i18n.setLocale(navigator.language);
          // TODO: set the lang attribute of html's root element
        });
      });
  });

  await aurelia.start();
  await aurelia.setRoot();
}
