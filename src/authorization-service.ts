import { AccessToken, AccessTokenInit, LocalAccount, RawAccessToken, LocalAccountInit } from "./local-account-model";
import environment from "./environment";
import { HttpClientConfiguration, HttpClient } from 'aurelia-fetch-client';
import * as store from 'store';
import { Account, RawAccount } from './account-model';
import { NewInstance } from 'aurelia-framework';
import AccountService from "./account-service";

export default class AuthorizationService {
  static inject = [
    HttpClientConfiguration,
    HttpClient,
    NewInstance.of(HttpClient),
    AccountService,
  ];
  static accountsKey = 'accounts';

  private config: HttpClientConfiguration;
  private http: HttpClient;
  private basicHttp: HttpClient;
  private accountService: AccountService;
  public accounts: LocalAccount[];

  constructor(
    config: HttpClientConfiguration,
    http: HttpClient,
    basicHttp: HttpClient,
    accountService: AccountService,
  ) {
    this.config = config;
    this.http = http;
    this.basicHttp = basicHttp;
    this.accountService = accountService;
    this.accounts = (store
      .get(AuthorizationService.accountsKey, []) as LocalAccountInit[])
      .map(init => new LocalAccount(init));
  }

  public async authorize(): Promise<void> {
    let localAccount = this.accounts.find(account => account.active);
    const authCode = this.extractAuthCode();
    if (authCode) {
      if (localAccount) {
        localAccount.active = false;
      }
      try {
        const userToken = await this.fetchUserToken(authCode);
        this.applyToken(new AccessToken(userToken));
        const account = await this.accountService.fetchActiveAccount();
        localAccount = new LocalAccount({ token: userToken, account, active: true });
        this.accounts.push(localAccount);
      } catch (error) {
        throw error;
      }
    }
    if (!localAccount) {
      try {
        const anonToken = await this.fetchUserLessToken();
        localAccount = new LocalAccount({ token: anonToken, account: null, active: true });
        this.applyToken(localAccount.token);
        this.accounts.push(localAccount);
      } catch (error) {
        throw error;
      }
    } else {
      this.applyToken(localAccount.token);
    }

    this.storeAccounts();

    this.http.configure((config) => {
      config.withInterceptor({
        request: this.checkTokens.bind(this),
      });
    });
  }

  private storeAccounts() {
    store.set(AuthorizationService.accountsKey, this.accounts);
  }

  private async checkTokens(request: Request): Promise<Request> {
    const activeAccount = this.accounts.find(account => account.active)!;
    let newRequest = request;

    if (activeAccount.token.isExpired()) {
      try {
        if (activeAccount.account === null) {
          const tokenInit = await this.fetchUserLessToken();
          const token = new AccessToken(tokenInit);
          activeAccount.token = token;
          this.applyToken(token);
          newRequest = new Request(request, {
            headers: new Headers(this.getHeaders(token)),
          });
        } else {
          const tokenInit = await this.refreshUserToken(activeAccount.token.refresh!);
          const token = new AccessToken(tokenInit);
          token.refresh = activeAccount.token.refresh;
          activeAccount.token = token;
          this.applyToken(token);
          newRequest = new Request(request, {
            headers: new Headers(this.getHeaders(token)),
          })
        }
      } catch (error) {
        throw error;
      }
      this.storeAccounts();
    }
    return newRequest;
  }

  private extractAuthCode(): string | undefined {
    const url = new URL(location.href);
    const searchParams = url.searchParams;
    const error = searchParams.get('error');
    const code = searchParams.get('code');
    // Remove the query parameters from current url.
    url.search = '';
    history.replaceState(null, '', url.toString());
    if (!error) {
      // If there is no error, code is never null.
      return code!;
    }
  }

  public requestUserToken(): never {
    const authorizeURL = new URL('https://www.reddit.com/api/v1/authorize.compact');
    authorizeURL.searchParams.set('client_id', environment.clientId);
    authorizeURL.searchParams.set('response_type', 'code');
    authorizeURL.searchParams.set('state', 'Romania');
    authorizeURL.searchParams.set('redirect_uri', 'http://localhost:9000/');
    authorizeURL.searchParams.set('duration', 'permanent');
    authorizeURL.searchParams.set('scope', '*');
    location.href = authorizeURL.toString();
  }

  private getHeaders(token: AccessToken): { [key: string]: string } {
    return {
      Authorization: `bearer ${token}`,
    };
  }

  private applyToken(token: AccessToken): void {
    this.http.configure((config) => {
      config
        .withBaseUrl('https://oauth.reddit.com/')
        .withDefaults({
          headers: this.getHeaders(token),
        });
    });
  }

  private async fetchUserLessToken(): Promise<AccessTokenInit> {
    const authStringEncoded = btoa(`${environment.clientId}:${environment.clientSecret}`);
    const headers = new Headers({
      Authorization: `Basic ${authStringEncoded}`,
    });
    const body = new URLSearchParams({
      grant_type: 'https://oauth.reddit.com/grants/installed_client',
      device_id: 'DO_NOT_TRACK_THIS_DEVICE',
    });
    try {
      const created = Date.now();
      const response = await this.basicHttp.fetch('https://www.reddit.com/api/v1/access_token', {
        method: 'POST',
        headers,
        body,
      });
      const rawAccessToken = (await response.json()) as RawAccessToken;
      return {
        token: rawAccessToken.access_token,
        created,
        lifespan: rawAccessToken.expires_in * 1000,
      };
    } catch (error) {
      throw error;
    }
  }

  private async fetchUserToken(code: string): Promise<AccessTokenInit> {
    const authStringEncoded = btoa(`${environment.clientId}:${environment.clientSecret}`);
    const headers = new Headers({
      Authorization: `Basic ${authStringEncoded}`,
    });
    const body = new URLSearchParams({
      grant_type: 'authorization_code',
      code,
      redirect_uri: 'http://localhost:9000/',
    });
    try {
      const created = Date.now();
      const response = await this.basicHttp.fetch('https://www.reddit.com/api/v1/access_token', {
        method: 'POST',
        headers,
        body,
      });
      const rawAccessToken = (await response.json()) as RawAccessToken;
      return {
        token: rawAccessToken.access_token,
        created,
        lifespan: rawAccessToken.expires_in * 1000,
        refresh: rawAccessToken.refresh_token,
      };
    } catch (error) {
      throw error;
    }
  }

  private async refreshUserToken(refreshToken: string): Promise<AccessTokenInit> {
    const authStringEncoded = btoa(`${environment.clientId}:${environment.clientSecret}`);
    const headers = new Headers({
      Authorization: `Basic ${authStringEncoded}`,
    });
    const body = new URLSearchParams({
      grant_type: 'refresh_token',
      refresh_token: refreshToken,
    });
    try {
      const created = Date.now();
      const response = await this.basicHttp.fetch('https://www.reddit.com/api/v1/access_token', {
        method: 'POST',
        headers,
        body,
      });
      const rawAccessToken = (await response.json()) as RawAccessToken;
      return {
        token: rawAccessToken.access_token,
        created,
        lifespan: rawAccessToken.expires_in * 1000,
      };
    } catch (error) {
      throw error;
    }
  }
}
