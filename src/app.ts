import { RouterConfiguration, Router, NavigationInstruction, Next } from 'aurelia-router';
import { AureliaUX } from "@aurelia-ux/core";

export default class {
  static inject = [AureliaUX];

  public router?: Router;

  constructor(ux: AureliaUX) {
    ux.design.primary = '#007bff';
    ux.design.accent = '#ff3d00';
  }

  configureRouter(config: RouterConfiguration, router: Router) {
    this.router = router;
    config.addPostRenderStep({
      run(navigationInstruction: NavigationInstruction, next: Next) {
        if (navigationInstruction.router.isNavigatingNew) {
          window.scroll(0, 0);
        }
        return next();
      }
    });
    /* eslint-disable no-param-reassign */
    config.options.pushState = true;
    config.title = 'thornbird';
    /* eslint-enable no-param-reassign */
    config.map([
      {
        route: '',
        moduleId: 'posts-wrapper',
        nav: true,
        title: 'Home',
        settings: { icon: 'home' },
        name: 'home',
      },
      {
        route: 'r/:sub/',
        moduleId: 'posts-wrapper',
        name: 'subreddit',
      },
      {
        route: 'r/:sub/',
        href: '/r/popular',
        moduleId: 'posts-wrapper',
        nav: true,
        title: 'Popular',
        settings: { icon: 'whatshot' },
        name: 'popular',
        generationUsesHref: true,
      },
      {
        route: 'r/:sub/',
        href: '/r/all',
        moduleId: 'posts-wrapper',
        nav: true,
        title: 'All',
        settings: { icon: 'list' },
        name: 'all',
        generationUsesHref: true,
      },
      {
        route: 'r/:sub?/comments/:id/:name?/',
        name: 'comments',
        moduleId: 'comments-wrapper',
      },
      {
        route: ['u/:name', 'user/:name/'],
        name: 'account',
        moduleId: 'account',
      },
      {
        route: 'nav',
        moduleId: 'side-nav',
        name: 'nav',
      },
      {
        route: 'license',
        moduleId: 'license',
        name: 'license',
        title: 'Licenses',
      },
      {
        route: 'login',
        moduleId: 'login',
        name: 'login',
        title: 'Login',
        nav: true,
      }
    ]);
  }
}

