import { HttpClient } from "aurelia-fetch-client";

export default class {
  static inject = [HttpClient];

  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  public async vote(id: string, dir: 1 | -1 | 0) {
    try {
      const query = new URLSearchParams({
        id,
        dir,
      });
      await this.http.fetch(`api/vote?${query}`, {
        method: 'POST',
      });
    } catch (error) {
      throw error;
    }
  }
}
