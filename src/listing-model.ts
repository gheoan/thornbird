/* eslint-disable camelcase */
import { Thing } from './thing-model';

export interface Listing<Thing> {
  data: {
    children: Thing[];
  }
}

export enum ListingTime {
  Hour = 'hour',
  Day = 'day',
  Week = 'week',
  Month = 'month',
  Year = 'year',
  All = 'all',
}
