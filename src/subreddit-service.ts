import { HttpClient } from "aurelia-fetch-client";
import { RawSubreddit, Subreddit } from './subreddit-model';
import { Thing } from './thing-model';
import { Listing } from './listing-model';

export default class {
  static inject = [HttpClient];

  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  public async fetchSubreddits(scope?: string): Promise<Array<Subreddit>> {
    const response = await this.http.fetch(`subreddits/${scope}`);
    const subredditListing: Listing<Thing<RawSubreddit>> = await response.json();
    return subredditListing.data.children.map(thing => new Subreddit(thing.data));
  }

  public async fetchSubredditByName(name: string): Promise<Subreddit> {
    try {
      const response = await this.http.fetch(`/r/${name}/about`);
      const rawPost: Thing<RawSubreddit> = await response.json();
      const post = new Subreddit(rawPost.data);
      return post;
    } catch (error) {
      throw error;
    }
  }
}
