import { HttpClient } from 'aurelia-fetch-client';
import { RawComment, Comment, CommentSort, CommentsParameters } from './comment-model';
import { Thing } from './thing-model';
import { Listing, ListingTime } from './listing-model';
import { RawPost, Post } from './post-model';

export default class {
  static inject = [HttpClient]
  private http: HttpClient;
  constructor(http: HttpClient) {
    this.http = http;
  }

  public async fetchComments({
    id,
    sort = CommentSort.Best,
  }: CommentsParameters): Promise<{ comments: Array<Comment>, post: Post }> {
    try {
      const searchParams = new URLSearchParams({
        sort,
      });
      const url = `comments/${id}?${String(searchParams)}`;
      const response = await this.http.fetch(url);
      const [
        { data: { children: [{ data: rawPost }] } },
        { data: { children: rawComments } },
      ]: [Listing<Thing<RawPost>>, Listing<Thing<RawComment>>] = await response.json();
      return {
        post: new Post(rawPost),
        comments: rawComments.map((thing: Thing<RawComment>) => new Comment(thing.data)),
      };
    } catch (error) {
      throw error;
    }
  }
}
