/* eslint-disable camelcase */
export interface RawSubreddit {
  display_name: string;
  url: string;
  id: string;
}

export class Subreddit {
  displayName: string;
  url: string;

  constructor(data: RawSubreddit) {
    this.displayName = data.display_name;
    this.url = data.url;
  }
}
