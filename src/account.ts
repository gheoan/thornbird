import { RoutableComponentActivate, RouteConfig } from 'aurelia-router';
import AccountService from './account-service';
import { Post } from './post-model';
import { Comment } from './comment-model';

export default class implements RoutableComponentActivate {
  static inject = [AccountService];

  private accountService: AccountService;
  public name?: string;
  public content?: Array<Post | Comment>;

  constructor(accountService: AccountService) {
    this.accountService = accountService;
  }

  async activate({ name }: { name: string }, config: RouteConfig): Promise<void> {
    // TODO: wait for both promises?
    const [account, content] = await Promise.all([
      this.accountService.fetchAccount(name),
      this.accountService.fetchAccountOverview(name),
    ]);

    this.name = account.name;
    config.navModel!.setTitle(this.name);
    this.content = content;
  }
}
