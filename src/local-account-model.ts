import { Account } from "./account-model";

export interface RawAccessToken {
  "access_token": string,
  "token_type": "bearer",
  "expires_in": number,
  "scope": string,
  refresh_token: string,
}

export interface AccessTokenInit {
  token: string,
  created: number,
  lifespan: number,
  refresh?: string,
}

export class AccessToken implements AccessTokenInit {
  static storeKey = 'access-token';

  public token: string;
  private created: number;
  /**
   * Duration in miliseconds for which the token is valid.
   */
  private lifespan: number;
  /**
   * A token that can be used to refresh the authentication token. null if the authentication token
   * can't be refreshed.
   */
  public refresh: string | null;

  constructor({token, created, lifespan, refresh = null}: AccessTokenInit) {
    this.token = token;
    this.created = created;
    this.lifespan = lifespan;
    this.refresh = refresh;
  }

  public get timeLeft(): number {
    return this.created + this.lifespan - Date.now();
  }

  public isExpired(): boolean {
    return this.timeLeft <= 0;
  }

  public toString(): string {
    return this.token;
  }
}

export interface LocalAccountInit {
  token: AccessTokenInit;
  account: Account | null;
  active?: boolean;
}

/**
 * An account that was stored locally after it's owner authorized this application too use it.
 */
export class LocalAccount {
  token: AccessToken;
  /**
   * The information asociated with this local account or null if the account is anonymous.
   */
  account: Account | null;
  active: boolean;

  constructor({ token, account, active = false }: LocalAccountInit) {
    this.token = new AccessToken(token);
    this.account = account;
    this.active = active;
  }
}
