import { RoutableComponentActivate } from 'aurelia-router';
import PostService from './post-service';
import { Post, PostsParameters } from './post-model';

export default class implements RoutableComponentActivate {
  public posts: Array<Post> | null = null;
  private service: PostService;

  static inject = [PostService];

  constructor(service: PostService) {
    this.service = service;
  }

  public async activate(parameters: PostsParameters) {
    try {
      this.posts = await this.service.fetchPosts(parameters);
    } catch (error) {
      // TODO: handle error
      throw error;
    }
  }
}
