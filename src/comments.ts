import CommentService from './comment-service';
import { Post } from './post-model';
import { Comment, CommentsParameters } from './comment-model';

export default class {
  private commentService: CommentService;
  public post?: Post;
  public comments?: Array<Comment>;
  static inject = [CommentService]

  constructor(commentService: CommentService) {
    this.commentService = commentService;
  }

  public async activate({ id, sort }: CommentsParameters) {
    const { post, comments } = await this.commentService.fetchComments({ id, sort });
    this.post = post;
    this.comments = comments;
  }
}
