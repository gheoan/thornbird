import { HttpClient } from 'aurelia-fetch-client';
import { RawAccount, Account } from './account-model';
import { Thing } from './thing-model';
import { RawPost, Post } from './post-model';
import { Listing } from './listing-model';
import { RawComment, Comment } from './comment-model';

export default class {
  static inject = [HttpClient]
  http: HttpClient;
  constructor(http: HttpClient) {
    this.http = http;
  }

  async fetchAccount(name: string): Promise<Account> {
    try {
      const url = `/user/${name}/about`;
      const response = await this.http.fetch(url);
      const accountThing: Thing<RawAccount> = await response.json();
      const rawAccount = accountThing.data;
      const account = new Account(rawAccount);
      return account;
    } catch (error) {
      throw error;
    }
  }

  async fetchAccountOverview(name: string): Promise<Array<Post | Comment>> {
    try {
      const url = `/user/${name}/overview`;
      const response = await this.http.fetch(url);
      const thing: Listing<Thing<RawPost> | Thing<RawComment>> = await response.json();
      const { children } = thing.data;
      return children.map((item) => {
        if (item.kind === 't1') {
          return new Comment(item.data as RawComment);
        }
        return new Post(item.data as RawPost);
      });
    } catch (error) {
      throw error;
    }
  }

  public async fetchActiveAccount(): Promise<Account> {
    try {
      const response = await this.http.fetch('api/v1/me');
      const json = await response.json() as RawAccount;
      return new Account(json);
    } catch (error) {
      throw error;
    }
  }
}
