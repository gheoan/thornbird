import { AppRouter, Router } from 'aurelia-router';
import { declarePropertyDependencies } from 'aurelia-framework';
import { PostSort } from './post-model';

const constructor = class {
  static inject = [Router];

  router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  get showTimeSort() {
    const { currentInstruction } = this.router;
    if (currentInstruction) {
      const { sort } = currentInstruction.params;
      if (sort) {
        return sort === PostSort.Top || sort === PostSort.Controversial;
      }
    }
    return false;
  }
};
declarePropertyDependencies(constructor, 'showTimeSort', ['router.currentInstruction']);
export default constructor;
