# Contributing guidelines

Thank you for considering contributing to this project.

Issues and merge requests must follow the **code of conduct**.

If it's your first time contributing to an open source project, read
[this guide](https://opensource.guide/how-to-contribute/).

## Issues

Issues is the place to report **bugs**, **request features**, ask questions, etc.

## Merge Requests

Merge requests must comply with [<cite>Reddit</cite>'s API terms](https://www.reddit.com/wiki/api).

There are no commit message conventions.

Following the project code style is optional. The `npx aurelia lint` command can be used to lint the
project.

## Development

The <cite>[Aurelia CLI](http://aurelia.io/docs/build-systems/aurelia-cli#running-your-aurelia-app)
</cite>is used to help with the development of the application.

1. Ensure that at least version 8.0.0 of <cite>[Node.js](https://nodejs.org/)</cite> is installed;
2. Ensure that at least version 5.2.0 of <cite>[npm](https://www.npmjs.com/)</cite> is installed;
3. Clone or download the repository locally;
4. From the project directory, execute `npm install`;
5. Execute `npx aurelia help` for help regarding building, running, testing, etc.

## Configuration

The <cite>OAuth</cite> client ID for <cite>Reddit</cite> API can be changed in the
`aurelia_project/environments/` directory.

## Deployment

After building, the `public/` directory will contain all the files that should be uploaded to the
web hosting service.

If you want to use <cite>[Firebase](https://firebase.google.com/)</cite> Hosting, it is already
configured, you can run `npx firebase login`, `npx firebase use project-id` and then
`npx firebase deploy`.

## Built With

- <cite>[Aurelia](http://aurelia.io/)</cite> - <cite>JavaScript</cite> client framework.

## License

This project is licensed under the <cite>Mozilla Public License Version 2.0</cite>
(<cite>MPL 2.0</cite>). See the [LICENSE](LICENSE) file or
[this FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/) for details. There is no additional
contributor agreement.
