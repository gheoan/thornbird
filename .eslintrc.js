module.exports = {
  root: true,
  extends: 'airbnb-base',
  parser: 'typescript-eslint-parser',
  rules: {
    // Parser's known issues.
    'no-undef': 'off',
    'no-unused-vars': 'off',
    'no-useless-constructor': 'off',
    'space-infix-ops': 'off',
    'no-restricted-globals': 'off',
    'no-empty-function': 'off',
    'import/no-unresolved': 'off',
    'import/extensions': 'off',
  }
}
