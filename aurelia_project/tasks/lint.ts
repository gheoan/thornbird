import * as gulp from 'gulp';
import * as eslint from 'gulp-eslint';
import * as project from '../aurelia.json';
import { CLIOptions } from 'aurelia-cli';
import * as htmllint from "gulp-htmllint";
import * as gulpIf from 'gulp-if';
import * as styleLint from "gulp-stylelint";

export default gulp.parallel(
  lintScript,
  lintMarkup,
  lintStyle,
);

function lintScript() {
  return gulp.src([project.transpiler.source])
    .pipe(eslint({ fix: CLIOptions.taskName() === 'lint' && CLIOptions.hasFlag('fix') }))
    .pipe(eslint.format())
    .pipe(gulpIf(file => file.eslint != null && file.eslint.fixed, gulp.dest(project.paths.root)))
    ;
}

function lintMarkup() {
  return gulp.src([project.markupProcessor.source, project.platform.index])
    .pipe(htmllint({ config: '.htmllintrc.json' }));
}

function lintStyle() {
  return gulp.src([project.cssProcessor.source])
    .pipe(styleLint({
      failAfterError: false,
      reporters: [{ formatter: 'string', console: true }]
    }))
    ;
}
