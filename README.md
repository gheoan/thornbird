# thornbird

<cite>thornbird</cite> is an **unofficial web client for <cite>Reddit</cite>**, meaning it can be
used instead of the official <cite>Reddit</cite> website or applications.

<cite>thornbird</cite>'s goal is to provide an alternative to the official clients that can be used
from any device, is fast and pleasant to use.

<cite>thornbird</cite> works by fetching the content from the official server and then displaying
it.

A **demo** is available [here](https://thornbird-c96ed.web.app/).

## Getting Started

1. Ensure that <cite>[Node.js](https://nodejs.org/)</cite> and
<cite>[npm](https://www.npmjs.com/)</cite> are installed;
2. Execute `npm install`;
3. Execute `npx aurelia help` for help regarding building, running, testing, etc.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details regarding contributing (building,
configuration, deploying, license, dependencies). Contributions must follow the code of conduct.
